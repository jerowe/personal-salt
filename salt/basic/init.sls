basic:
    pkg:
        - installed
        - names:
            - git
            - gitflow
            - guake
            - openssh-server
            - openssh-clients
            - curl
            - tmux
            - environment-modules
            - zsh
            - vim-enhanced
            - openssl-devel 
            - readline-devel 
            - bzip2-devel 
            - libsqlite3x-devel
            - sqlite2-devel 
            - zlib-devel
            - patch 
            - gnome-tweak-tool 

dev:
    pkg:
        - installed
        - names:
            - gcc
            - gcc-c++
            - gdb
            - cmake
            - automake
            - libtool
            - clang
            - gcc-gfortran
            - parallel
            - blas-devel
            - atlas-devel
            - lapack-devel

write:
    pkg:
        - installed
        - names:
            - texlive 
            - texlive-latex 
            - texlive-autopdf 
            - texlive-xetex
            - texlive-collection-latex
            - texlive-collection-latexrecommended
            - texlive-xetex-def
            - texlive-collection-xetex
            - pandoc
