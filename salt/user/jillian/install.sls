{% set current_path = salt['environ.get']('PATH', '/bin:/usr/bin') %}
{% set anyenv_root = salt['pillar.get']('anyenv:lookup:root') %}

#spf14:
#  cmd.run:
#    - name: curl http://j.mp/spf13-vim3 -L -o - | sh && vim +BundleInstall! +BundleClean +q 
#    - user: jillian
#    - cwd: /home/jillian
#
#ohmyzsh:
#  cmd.run:
#    - name: wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O - | sh  
#    - user: jillian
#    - cwd: /home/jillian
#
#zsh-antigen:
#  cmd.run:
#    - name: git clone https://github.com/zsh-users/antigen .antigen
#    - user: jillian
#    - cwd: /home/jillian
#
#tmuxifier:
#  cmd.run:
#    - name: git clone https://github.com/jimeh/tmuxifier .tmuxifier
#    - user: jillian
#    - cwd: /home/jillian
#
#anyenv-install:
#  cmd.run:
#    - name: git clone https://github.com/riywo/anyenv .anyenv; echo 'export PATH="$HOME/.anyenv/bin:$PATH"' >> .zshrc; echo 'eval "$(anyenv init -)"' >> ~/.zshrc && source .zshrc 
#    - user: jillian
#    - cwd: /home/jillian
#
#anyenv-envs:
#   cmd.run:
#     - name: anyenv install plenv; anyenv install pyenv; anyenv install nodenv
#     - user: jillian
#     - cwd: /home/jillian
#     - env:
#      - PATH: {{ ['/home/jillian/.anyenv/bin:/home/jillian/.anyenv/shims', current_path]|join(':') }}
#      - ANYENV_ROOT: /home/jillian/.anyenv

#anaconda3:
#  cmd.run:
#    - name: pyenv install anaconda3-2.4.1 
#    - user: jillian
#    - cwd: /home/jillian
#    - env:
#      - PATH: {{ ['/home/jillian/.anyenv/bin:/home/jillian/.anyenv/shims:/home/jillian/.anyenv/envs/pyenv/bin:/home/jillian/.anyenv/envs/pyenv/shims', current_path]|join(':') }}
#      - ANYENV_ROOT: {{ salt['pillar.get']('anyenv:lookup:root') }}  
#      - PYENV_ROOT: /home/jillian/.anyenv/envs/pyenv

which_python:
  cmd.run:
    - name: which python 
    - user: jillian
    - cwd: /home/jillian
    - env:
      - PATH: {{ ['/home/jillian/.anyenv/bin:/home/jillian/.anyenv/shims:/home/jillian/.anyenv/envs/pyenv/bin:/home/jillian/.anyenv/envs/pyenv/shims', current_path]|join(':') }}
      - ANYENV_ROOT: {{ salt['pillar.get']('anyenv:lookup:root') }}  
      - PYENV_ROOT: /home/jillian/.anyenv/envs/pyenv
      - PYENV_VERSION: anaconda3-2.4.1 

#pyenv_scipy:
#  cmd.run:
#    - name: /home/jillian/.anyenv/envs/pyenv/shims/pip install scipy
#    - user: jillian
#    - cwd: /home/jillian
#    - env:
#      - PATH: {{ ['/home/jillian/.anyenv/bin:/home/jillian/.anyenv/shims:/home/jillian/.anyenv/envs/pyenv/bin:/home/jillian/.anyenv/envs/pyenv/shims', current_path]|join(':') }}
#      - ANYENV_ROOT: /home/jillian/.anyenv
#      - PYENV_ROOT: /home/jillian/.anyenv/envs/pyenv
#      - PYENV_VERSION: anaconda3-2.4.1 
#      - LAPACK: /usr/lib64/libpack.so   
#      - BLAS: /usr/lib64/libblas.so    
#      - ATLAS: /usr/lib64/atlas/libatlas.so
#
