/home/jillian/.vimrc.local:
  file.managed:
    - source: salt://user/jillian/.vimrc.local
    - user: jillian 
    - group: jillian 
    - mode: 644

/home/jillian/.tmux.conf:
  file.managed:
    - source: salt://user/jillian/.tmux.conf
    - user: jillian 
    - group: jillian 
    - mode: 644

/home/jillian/.vimrc.bundles.local:
  file.managed:
    - source: salt://user/jillian/.vimrc.bundles.local
    - user: jillian 
    - group: jillian 
    - mode: 644

/home/jillian/.vimrc.before.local:
  file.managed:
    - source: salt://user/jillian/.vimrc.before.local
    - user: jillian 
    - group: jillian 
    - mode: 644

/home/jillian/.zshrc:
  file.managed:
    - source: salt://user/jillian/.zshrc
    - user: jillian 
    - group: jillian 
    - mode: 644

/home/jillian/.ctags:
  file.managed:
    - source: salt://user/jillian/.ctags
    - user: jillian 
    - group: jillian 
    - mode: 644
